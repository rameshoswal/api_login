# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Menu.create(item: 'Roti',price: 123)
Menu.create(item: 'kulcha',price: 1)
Menu.create(item: 'sabji1',price: 2)
Menu.create(item: 'sabji2',price: 3)
