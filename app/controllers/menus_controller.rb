class MenusController < ApplicationController
before_action :ensure_auth!
	def index
		if current_user
			menu = Menu.all
			render json: menu
		else
			render json: {},status: :unauthorized
		end
	

	end

end