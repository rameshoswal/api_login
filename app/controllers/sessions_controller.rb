class SessionsController < Devise::SessionsController
	before_action :ensure_auth!, only: [:destroy_session]
	skip_before_filter :verify_authenticity_token, :only => [:destroy_session]
	def create
		user = User.find_for_database_authentication(email: params[:email])
		
		if user && user.valid_password?(params[:password])
			token = user.ensure_authentication_token
			render json: {auth_token: token}
			
			
		else
			render nothing: true, status: :unauthorized	
		end
	end
	def destroy_session
		puts current_user.email
		if current_user
			current_user.destroy_auth_token
			render json: {data: "deleted"}
		else
			render nothing: true, status: :unauthorized
		end
	end

end